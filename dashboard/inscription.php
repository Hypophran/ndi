<!DOCTYPE html>
<html>
    <title>Inscription</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <body>
        <?php
            if (!isset($_POST['email']) || !isset($_POST['confirmeremail']) || !isset($_POST['pass']) || !isset($_POST['confirmerpass']) || !isset($_POST['nom']) || !isset($_POST['prenom']) || !isset($_POST['tel'])){
                echo "<div class=\"w3-container\">
                            <div id=\"id01\" class=\"w3-modal\" style='display:block'>
                                <div class=\"w3-modal-content w3-card-4 w3-animate-zoom\" style=\"max-width:600px\">
                                    <div class=\"w3-center\"><br>
                                        <h1>Inscription</h1>
                                    </div>
                                    <form class=\"w3-container\" method=\"post\" name=\"frm\" action=\"inscription.php\">
                                        <div class=\"w3-section\">
                                            <label><b>Adresse Mail</b></label>
                                            <input class=\"w3-input w3-border w3-margin-bottom\" type=\"email\" placeholder=\"Entrer l'adresse mail\" name=\"email\" required>
                                            <label><b>Confirmer Adresse Mail</b></label>
                                            <input class=\"w3-input w3-border w3-margin-bottom\" type=\"email\" placeholder=\"Entrer l'adresse mail\" name=\"confirmeremail\" required>
                                            <label><b>Mot de Passe</b></label>
                                            <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"pass\" required>
                                            <label><b>Confirmer Mot de Passe</b></label>
                                            <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"confirmerpass\" required>
                                            <label><b>Nom</b></label>
                                            <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le nom\" name=\"nom\" required>
                                            <label><b>Prenom</b></label>
                                            <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le prenom\" name=\"prenom\" required>
                                            <label><b>Telephone</b></label>
                                            <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le telephone\" name=\"tel\" required>
                                            <button class=\"w3-button w3-block w3-green w3-section w3-padding\" type=\"submit\" name=\"valider\">Valider</button>
                                        </div>
                                    </form>
                                    <div class=\"w3-container w3-border-top w3-padding-16 w3-light-grey\">
                                        <span class=\"w3-left w3-padding w3-hide-small\"><a href=\"login.php\">Retourner a la page de connexion</a></span>
                                        <span class=\"w3-right w3-padding w3-hide-small\"><a href=\"#\">Mot de passe oublie ?</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>";
            }
            else { 
                if ($_POST['email'] != $_POST['confirmeremail']){
                    echo "<div class=\"w3-container\">
                            <div id=\"id01\" class=\"w3-modal\" style='display:block'>
                                <div class=\"w3-modal-content w3-card-4 w3-animate-zoom\" style=\"max-width:600px\">
                                    <div class=\"w3-center\"><br>
                                        <h1>Inscription</h1>
                                        <p>Les mots de passe rentres sont differents !</p>
                                    </div>
                                    <form class=\"w3-container\" method=\"post\" name=\"frm\" action=\"inscription.php\">
                                        <div class=\"w3-section\">
                                            <label><b>Adresse Mail</b></label>
                                            <input class=\"w3-input w3-border w3-margin-bottom\" type=\"email\" placeholder=\"Entrer l'adresse mail\" name=\"email\" required>
                                            <label><b>Confirmer Adresse Mail</b></label>
                                            <input class=\"w3-input w3-border w3-margin-bottom\" type=\"email\" placeholder=\"Entrer l'adresse mail\" name=\"confirmeremail\" required>
                                            <label><b>Mot de Passe</b></label>
                                            <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"pass\" required>
                                            <label><b>Confirmer Mot de Passe</b></label>
                                            <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"confirmerpass\" required>
                                            <label><b>Nom</b></label>
                                            <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le nom\" name=\"nom\" required>
                                            <label><b>Prenom</b></label>
                                            <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le prenom\" name=\"prenom\" required>
                                            <label><b>Telephone</b></label>
                                            <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le telephone\" name=\"tel\" required>
                                            <button class=\"w3-button w3-block w3-green w3-section w3-padding\" type=\"submit\" name=\"valider\">Valider</button>
                                        </div>
                                    </form>
                                    <div class=\"w3-container w3-border-top w3-padding-16 w3-light-grey\">
                                        <span class=\"w3-left w3-padding w3-hide-small\"><a href=\"login.php\">Retourner a la page de connexion</a></span>
                                        <span class=\"w3-right w3-padding w3-hide-small\"><a href=\"#\">Mot de passe oublie ?</a></span>
                                    </div>
                                </div>
                            </div>
                        </div>";
                }
                else if ($_POST['pass'] != $_POST['confirmerpass']){
                    echo "<div class=\"w3-container\">
                          <div id=\"id01\" class=\"w3-modal\" style='display:block'>
                            <div class=\"w3-modal-content w3-card-4 w3-animate-zoom\" style=\"max-width:600px\">
                                <div class=\"w3-center\"><br>
                                    <h1>Inscription</h1>
                                    <p>Les mots de passe rentres sont differents !</p>
                                </div>
                                <form class=\"w3-container\" method=\"post\" name=\"frm\" action=\"inscription.php\">
                                    <div class=\"w3-section\">
                                        <label><b>Adresse Mail</b></label>
                                        <input class=\"w3-input w3-border w3-margin-bottom\" type=\"email\" placeholder=\"Entrer l'adresse mail\" name=\"email\" required>
                                        <label><b>Confirmer Adresse Mail</b></label>
                                        <input class=\"w3-input w3-border w3-margin-bottom\" type=\"email\" placeholder=\"Entrer l'adresse mail\" name=\"confirmeremail\" required>
                                        <label><b>Mot de Passe</b></label>
                                        <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"pass\" required>
                                        <label><b>Confirmer Mot de Passe</b></label>
                                        <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"confirmerpass\" required>
                                        <label><b>Nom</b></label>
                                        <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le nom\" name=\"nom\" required>
                                        <label><b>Prenom</b></label>
                                        <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le prenom\" name=\"prenom\" required>
                                        <label><b>Telephone</b></label>
                                        <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le telephone\" name=\"tel\" required>
                                        <button class=\"w3-button w3-block w3-green w3-section w3-padding\" type=\"submit\" name=\"valider\">Valider</button>
                                    </div>
                                </form>
                                <div class=\"w3-container w3-border-top w3-padding-16 w3-light-grey\">
                                    <span class=\"w3-left w3-padding w3-hide-small\"><a href=\"login.php\">Retourner a la page de connexion</a></span>
                                    <span class=\"w3-right w3-padding w3-hide-small\"><a href=\"#\">Mot de passe oublie ?</a></span>
                                </div>
                            </div>
                        </div>
                    </div>";
                }else {
                    $db = new PDO('mysql:host=localhost;dbname=ndi;charset=utf8', 'root', '');
                    $requete = $db->prepare("SELECT COUNT(*) FROM PERSONNE WHERE MAIL = :mail;");
                    $requete->execute(array('mail'=>$_POST['emai']));
                    $data = $requete->fetch();
                    if ($data[0] > 0){
                        echo "<div class=\"w3-container\">
                          <div id=\"id01\" class=\"w3-modal\" style='display:block'>
                            <div class=\"w3-modal-content w3-card-4 w3-animate-zoom\" style=\"max-width:600px\">
                                <div class=\"w3-center\"><br>
                                    <h1>Inscription</h1>
                                    <p>Les mots de passe rentres sont differents !</p>
                                </div>
                                <form class=\"w3-container\" method=\"post\" name=\"frm\" action=\"inscription.php\">
                                    <div class=\"w3-section\">
                                        <label><b>Adresse Mail</b></label>
                                        <input class=\"w3-input w3-border w3-margin-bottom\" type=\"email\" placeholder=\"Entrer l'adresse mail\" name=\"email\" required>
                                        <label><b>Confirmer Adresse Mail</b></label>
                                        <input class=\"w3-input w3-border w3-margin-bottom\" type=\"email\" placeholder=\"Entrer l'adresse mail\" name=\"confirmeremail\" required>
                                        <label><b>Mot de Passe</b></label>
                                        <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"pass\" required>
                                        <label><b>Confirmer Mot de Passe</b></label>
                                        <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"confirmerpass\" required>
                                        <label><b>Nom</b></label>
                                        <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le nom\" name=\"nom\" required>
                                        <label><b>Prenom</b></label>
                                        <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le prenom\" name=\"prenom\" required>
                                        <label><b>Telephone</b></label>
                                        <input class=\"w3-input w3-border\" type=\"text\" placeholder=\"Entrer le telephone\" name=\"tel\" required>
                                        <button class=\"w3-button w3-block w3-green w3-section w3-padding\" type=\"submit\" name=\"valider\">Valider</button>
                                    </div>
                                </form>
                                <div class=\"w3-container w3-border-top w3-padding-16 w3-light-grey\">
                                    <span class=\"w3-left w3-padding w3-hide-small\"><a href=\"login.php\">Retourner a la page de connexion</a></span>
                                    <span class=\"w3-right w3-padding w3-hide-small\"><a href=\"#\">Mot de passe oublie ?</a></span>
                                </div>
                            </div>
                        </div>
                    </div>";
                    }
                    else {
                        $requete = $db->prepare("INSERT INTO personne (mail,motdepasse,nom,prenom,telephone) VALUES (:email,:pass,:nom,:prenom,:telephone)");
                        $requete->execute(array('email'=>$_POST['email'],'pass'=>$_POST['pass'], 'nom'=>$_POST['nom'], 'prenom'=>$_POST['prenom'], 'telephone' => $_POST['tel']));
                        header("Location: login.php");
                        exit;
                    }
                }
            } 
            
        ?>
    </body>
</html>
