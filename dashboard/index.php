<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <title>Index</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <body>
    <?php
        if(!isset($_SESSION['mail'])){
            header("Location: login.php");
            exit;
        }
        else{
            header("Location: agenda.php");
            exit;
        }
    ?>
    </body>
</html>