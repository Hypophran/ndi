<?php
    session_start();
?>
<!DOCTYPE html>
<html>
    <title>Login</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <body>
        <?php
            if(isset($_POST['email']) && isset($_POST['pass'])){
                $db = new PDO('mysql:host=localhost;dbname=ndi;charset=utf8', 'root', '');
                $requete = $db->prepare("SELECT COUNT(*) FROM PERSONNE WHERE MAIL = :mail AND MOTDEPASSE = :pass;");
                $requete->execute(array('mail' => $_POST['email'], 'pass' => $_POST['pass']));
                $data = $requete->fetch();
                if($data[0] == 1){
                    $_SESSION['mail'] = $_POST['email'];
                    header("Location: index.php");
                    exit;
                }
                else {
                    echo"<div class=\"w3-container\">
                <div id=\"id01\" class=\"w3-modal\" style='display:block'>
                    <div class=\"w3-modal-content w3-card-4 w3-animate-zoom\" style=\"max-width:600px\">
                        <div class=\"w3-center\"><br>
                            <h1>Login</h1>
                            <p>L'identifiant et le mot de passe sont inconnus</p>
                        </div>
                        <form class=\"w3-container\" method=\"post\" name=\"frm\" action=\"login.php\">
                            <div class=\"w3-section\">
                                <label><b>Adresse Mail</b></label>
                                <input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" placeholder=\"Entrer l'adresse mail\" name=\"email\" required>
                                <label><b>Mot de passe</b></label>
                                <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"pass\" required>
                                <button class=\"w3-button w3-block w3-green w3-section w3-padding\" type=\"submit\" name=\"Valider\">Valider</button>
                            </div>
                        </form>
                        <div class=\"w3-container w3-border-top w3-padding-16 w3-light-grey\">
                            <span class=\"w3-left w3-padding w3-hide-small\"><a href=\"inscription.php\">S'inscrire</a></span>
                            <span class=\"w3-right w3-padding w3-hide-small\"><a href=\"#\">Mot de passe oublie ?</a></span>
                        </div>
                    </div>
                </div>
            </div>";
                }
            }
            else {
                echo"<div class=\"w3-container\">
                <div id=\"id01\" class=\"w3-modal\" style='display:block'>
                    <div class=\"w3-modal-content w3-card-4 w3-animate-zoom\" style=\"max-width:600px\">
                        <div class=\"w3-center\"><br>
                            <h1>Login</h1>
                        </div>
                        <form class=\"w3-container\" method=\"post\" name=\"frm\" action=\"login.php\">
                            <div class=\"w3-section\">
                                <label><b>Adresse Mail</b></label>
                                <input class=\"w3-input w3-border w3-margin-bottom\" type=\"text\" placeholder=\"Entrer l'adresse mail\" name=\"email\" required>
                                <label><b>Mot de passe</b></label>
                                <input class=\"w3-input w3-border\" type=\"password\" placeholder=\"Entrer le mot de passe\" name=\"pass\" required>
                                <button class=\"w3-button w3-block w3-green w3-section w3-padding\" type=\"submit\" name=\"Valider\">Valider</button>
                            </div>
                        </form>
                        <div class=\"w3-container w3-border-top w3-padding-16 w3-light-grey\">
                            <span class=\"w3-left w3-padding w3-hide-small\"><a href=\"inscription.php\">S'inscrire</a></span>
                            <span class=\"w3-right w3-padding w3-hide-small\"><a href=\"#\">Mot de passe oublie ?</a></span>
                        </div>
                    </div>
                </div>
            </div>";
            }
        ?>
    </body>
</html>
