<?php
class DataHandler {
	private $_file;

	public function __construct($file) {
		$this->_file = $file;
	}	

	public function getFile() {
		return $this->_file;
	}

	public function setFile($_file) {
		$this->_file = $file;
	}

	public function deserialize() {
		if(file_exists($this->getFile())) {
			$content = file_get_contents($this->getFile());
			return (array) json_decode($content);
		}
	}
}
?>