<?php

class UserHandler extends Handler {
	
	public function add(User $user) {
		
	}

	public function get($email, $pass) {
		$query = $this->getDb()->prepare('SELECT * FROM personne WHERE mail = :email AND motdepasse = :pass');
		$query->execute(array(
			"email" => $email,
			"pass" => hash("sha256", $pass)
		));
		if($query->rowCount() === 1) {
			$_SESSION['authenticated'] = true;
			$_SESSION['email'] = $email;
		} else {
			$_SESSION['authenticated'] = false;
		}
		$query->closeCursor();
	}
}

?>