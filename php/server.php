<?php
session_start();

try {
	$db = new PDO('mysql:host=localhost;dbname=ndi;charset=utf8', 'ndi', 'LoveJacques31');
} catch(Exception $e) {
	$query = new Query("mysql");
	$query->setFeedback("Erreur");
	$query->addError("Impossible de se connecter à la base de données.");
	$query->send();
}

spl_autoload_register('loadClass');

$query = isset($_GET['q']) ? $_GET['q'] : NULL;

if($query) {
	$query = new Query($query);
	$queryHandler = new QueryHandler($db);
	$queryHandler->process($query);
}

function loadClass($class) {
	require_once($class.'.php');
}
?>