<?php
	class Handler {
		protected $_db;

		public function __construct($db) {
			$this->_db = $db;
		}

		public function getDb() {
			return $this->_db;
		}

		public function setDb($db) {
			$this->_db = $db;
		}
	}
?>	