function tmp() {
$("form").submit(function(e){
	e.preventDefault();
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/php/chatbot/MessageHandler.php?q=" + document.getElementById("input-text").value, true);
    document.getElementById("main").innerHTML += '<div class="container darker"><p>' + document.getElementById("input-text").value + '</p><span class="time-right">4:20</span></div>';
	xhr.onreadystatechange = function(event) {
    // XMLHttpRequest.DONE === 4
    if (this.readyState === XMLHttpRequest.DONE) {
        if (this.status === 200) {
    		document.getElementById("main").innerHTML += '<div class="container"><p>' + this.responseText + '</p><span class="time-left">4:20</span></div>';
            document.getElementById("input-text").value = "";
            document.getElementById("input-text").scrollIntoView();
    		//tmp();
        } else {
            console.log("Status de la réponse: %d (%s)", this.status, this.statusText);
        }
    }
};
	
	xhr.send(null);
});
}
tmp();